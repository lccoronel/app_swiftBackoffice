import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './pages/Login';
import Main from './pages/Main';
import Coupon from './pages/Coupon';
import Holiday from './pages/Holiday';
import Users from './pages/Users';
import QrCode from './pages/QrCode';
import Message from './pages/Message';
import Notification from './pages/Notification';
import Store from './pages/Store';
import Manager from './pages/Manager';

const AppStack = createStackNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <AppStack.Navigator screenOptions={{ headerShown: false }} mode="modal">
        <AppStack.Screen name="Login" component={Login} />
        <AppStack.Screen name="Main" component={Main} />
        <AppStack.Screen name="Coupon" component={Coupon} />
        <AppStack.Screen name="Holiday" component={Holiday} />
        <AppStack.Screen name="Users" component={Users} />
        <AppStack.Screen name="QrCode" component={QrCode} />
        <AppStack.Screen name="Message" component={Message} />
        <AppStack.Screen name="Notification" component={Notification} />
        <AppStack.Screen name="Store" component={Store} />
        <AppStack.Screen name="Manager" component={Manager} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}
