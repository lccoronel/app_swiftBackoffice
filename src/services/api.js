import axios from 'axios';

const api = axios.create({
  baseURL: 'http://ec2-54-82-191-201.compute-1.amazonaws.com',
});

export default api;
