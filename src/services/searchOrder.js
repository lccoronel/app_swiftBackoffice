import { AsyncStorage } from 'react-native';
import api from './api';
import { toastError } from '../components/Toastify';

async function searchOrder(order) {
  if (!order) {
    return toastError('Informe um numero de pedido!');
  }

  try {
    const token = await AsyncStorage.getItem('token');

    const response = await api.post(
      `/coupons/order/${order}`,
      {},
      {
        headers: {
          Authorization: `JWT ${token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    return toastError('Pedido não encontrado, tente novamente!');
  }
}

export default searchOrder;
