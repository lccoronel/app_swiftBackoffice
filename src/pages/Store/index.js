/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import { AsyncStorage, ActivityIndicator, View } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import logoImg from '../../assets/logoSwift.png';
import searchOrder from '../../services/searchOrder';
import api from '../../services/api';
import {
  Container,
  Header,
  Title,
  Back,
  Search,
  Input,
  Button,
  TextButton,
  Order,
  ResultOrder,
  TitleOrder,
  SubTitle,
  ContainerStore,
  ListStore,
  Logo,
  Void,
} from './styles';

const Store = () => {
  const navigation = useNavigation();

  const [order, setOrder] = useState('');
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [show, setShow] = useState(false);
  const [result, setResult] = useState('');
  const [stories, setStories] = useState([]);

  async function handleSearchOrder() {
    setLoadingSearch(true);
    const response = await searchOrder(order);

    if (response) {
      setResult(response);
      setShow(true);

      const token = await AsyncStorage.getItem('token');

      const stores = await api.get(`stores/postal/${order}`, {
        headers: {
          Authorization: `JWT ${token}`,
        },
      });

      setStories(stores.data.stores);
      setLoadingSearch(false);
    }
    setLoadingSearch(false);
  }

  return (
    <Container keyboardShouldPersistTaps="handled">
      <Header>
        <Title>Lojas</Title>
        <Back onPress={() => navigation.goBack()}>
          <FontAwesome5
            name="arrow-left"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Search>
        <Input
          placeholder="Numero do pedido"
          value={order}
          onChangeText={(value) => setOrder(value)}
        />
        <Button loadingSearch={loadingSearch} onPress={handleSearchOrder}>
          {loadingSearch ? (
            <ActivityIndicator color="#484747" />
          ) : (
            <TextButton>Confirmar</TextButton>
          )}
        </Button>
      </Search>

      {show === false ? (
        <></>
      ) : (
        <Order>
          <TitleOrder>Documento</TitleOrder>
          <ResultOrder>{result.document}</ResultOrder>
          <TitleOrder>Nome</TitleOrder>
          <ResultOrder>{result.name}</ResultOrder>
          <TitleOrder>Pedido</TitleOrder>
          <ResultOrder>{result.orderId}</ResultOrder>
        </Order>
      )}

      <SubTitle>Lojas ({stories.length})</SubTitle>

      {stories.length === 0 ? (
        <Void>Nenhuma loja</Void>
      ) : (
        stories.map((list) => (
          <ContainerStore>
            <Logo source={logoImg} style={{ resizeMode: 'contain' }} />
            <ListStore>{list}</ListStore>
          </ContainerStore>
        ))
      )}

      <View style={{ marginBottom: 100 }} />
    </Container>
  );
};

export default Store;
