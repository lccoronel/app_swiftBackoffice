import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { darken } from 'polished';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Back = styled(RectButton)``;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#484747')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Text = styled.Text`
  color: #ffb800;
  padding: 5px 5px;
  margin-top: 10px;
`;

export const Manager = styled(RectButton)``;

export const Label = styled.Text`
  color: #ffb800;
  padding: 15px;
  margin-top: 10px;
  font-size: 17px;
  padding-bottom: 22px;
`;

export const Button = styled(RectButton)`
  background: #ffb800;
  height: 45px;
  width: 30%;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 4px;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

export const Div = styled.View`
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

export const ContainerCheckbox = styled.View`
  flex-direction: row;
  align-items: center;
`;
