/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import { AsyncStorage, ActivityIndicator } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Checkbox } from 'react-native-paper';

import {
  Container,
  Title,
  Header,
  Back,
  Input,
  Text,
  Manager,
  Label,
  Div,
  Button,
  TextButton,
  ContainerCheckbox,
} from './styles';
import api from '../../services/api';
import { toastSucess, toastError } from '../../components/Toastify';

const Users = () => {
  const navigation = useNavigation();
  const [manager, setManager] = useState(false);
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordAgain, setPasswordAgain] = useState('');
  const [email, setEmail] = useState('');
  const [firstName, setFirstname] = useState('');
  const [lastName, setLastName] = useState('');

  async function handleSubmit() {
    if (password.length >= 8 && passwordAgain === password) {
      setLoading(true);
      try {
        const token = await AsyncStorage.getItem('token');

        const response = await api.post(
          '/users/create',
          {
            username,
            password,
            email,
            firstName,
            lastName,
            isManager: manager,
          },
          {
            headers: {
              Authorization: `JWT ${token}`,
            },
          }
        );

        console.log(response.data);
        toastSucess('Usuário criado');
        setLoading(false);
        navigation.goBack();
      } catch (err) {
        toastError('Usuário não criado, tente novamente');
        setLoading(false);
      }
    } else {
      toastError('Dados fantando ou incorretos');
    }
  }

  return (
    <Container eyboardShouldPersistTaps="handled">
      <Header>
        <Title>Usuários</Title>
        <Back onPress={() => navigation.goBack()}>
          <FontAwesome5
            name="arrow-left"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Input
        placeholder="Nome do Usuario (username)"
        autoCorrect={false}
        value={username}
        onChangeText={(value) => setUsername(value)}
      />
      <Input
        placeholder="Senha"
        autoCorrect={false}
        value={password}
        onChangeText={(value) => setPassword(value)}
        secureTextEntry
      />
      <Input
        placeholder="Digite a senha novamente"
        autoCorrect={false}
        value={passwordAgain}
        onChangeText={(value) => setPasswordAgain(value)}
        secureTextEntry
      />
      <Text>* Obs: A senha deve conter no minímo 8 caracteres!</Text>
      <Input
        placeholder="Email"
        autoCorrect={false}
        value={email}
        onChangeText={(value) => setEmail(value)}
      />
      <Input
        placeholder="Primeiro Nome"
        autoCorrect={false}
        value={firstName}
        onChangeText={(value) => setFirstname(value)}
      />
      <Input
        placeholder="Segundo Nome"
        autoCorrect={false}
        value={lastName}
        onChangeText={(value) => setLastName(value)}
      />

      <ContainerCheckbox>
        <Checkbox
          status={manager ? 'checked' : 'unchecked'}
          onPress={() => setManager(!manager)}
          color="#ffb800"
          uncheckedColor="#ffb800"
        />
        <Manager onPress={() => setManager(!manager)}>
          <Label>Para usuario GESTOR aperte aqui</Label>
        </Manager>
      </ContainerCheckbox>

      <Div style={{ marginBottom: 100 }}>
        <Button loading={loading} onPress={handleSubmit}>
          {loading ? (
            <ActivityIndicator color="#484747" />
          ) : (
            <TextButton>Criar</TextButton>
          )}
        </Button>
      </Div>
    </Container>
  );
};

export default Users;
