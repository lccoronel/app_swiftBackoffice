import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { TextInputMask } from 'react-native-masked-text';
import { RectButton } from 'react-native-gesture-handler';
import { darken } from 'polished';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Back = styled(RectButton)``;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Info = styled.Text`
  background: #ffb800;
  padding: 15px;
  margin-top: 10%;
  font-size: 16px;
`;

export const Bold = styled.Text`
  font-weight: bold;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  background: ${darken(0.1, '#484747')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Date = styled(TextInputMask).attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#484747')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Button = styled(RectButton)`
  background: #ffb800;
  height: 45px;
  width: 30%;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 4px;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;
