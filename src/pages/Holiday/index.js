/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';

import { toastError, toastSucess } from '../../components/Toastify';

import {
  Container,
  Title,
  Header,
  Back,
  Button,
  TextButton,
  Input,
  Date,
  Info,
  Bold,
} from './styles';

const Holiday = () => {
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const [name, setName] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');

  async function handleHoliday() {
    setLoading(true);

    try {
      await axios.put(
        `https://swiftbr.vtexcommercestable.com.br/api/logistics/pvt/configuration/holidays/1q2w3e4r5t6y7u8i9o0p`,
        {
          name,
          startDate: `${startDate.substring(0, 4)}-${startDate.substring(
            5,
            7
          )}-${startDate.substring(8, 10)}`,
          endDate: `${endDate.substring(0, 4)}-${endDate.substring(
            5,
            7
          )}-${endDate.substring(8, 10)}`,
        },
        {
          headers: {
            'x-vtex-api-appKey': 'vtexappkey-swiftbr-BOKKFH',
            'x-vtex-api-appToken':
              'FCCXOCLFDWLJMEFHJOBLGXPFGPVTOAKGRJEKBDSNDQHCVQHJZVVTLNCHMXQZGSQCMFSCZVFFWNHPPNJQHLTSERUDHWYEMTGLZEPQYFJEQQXXFMSYNARUYYYVKOUDWBSC',
          },
        }
      );
      toastSucess('Feriado cadastrado!');
      setLoading(false);
      navigation.goBack();
    } catch (err) {
      toastError('Feriado não cadastrado, tente novamente');
      setLoading(false);
    }
  }

  return (
    <Container keyboardShouldPersistTaps="handled">
      <Header>
        <Title>Feriado</Title>
        <Back onPress={() => navigation.navigate('Main')}>
          <FontAwesome5
            name="arrow-left"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Info>
        <Bold>Como funciona?</Bold> Ao cadastrar um feriado o sistema Logistc`s
        não irá considerar esta data no cálculo de entrega. Não ė contado como
        dia útil.
      </Info>

      <Input
        placeholder="Nome do feriado"
        value={name}
        onChangeText={(value) => setName(value)}
      />
      <Date
        type="datetime"
        options={{ format: 'YYYY/MM/DD' }}
        placeholder="Data inicial (aaaa/mm/dd)"
        value={startDate}
        onChangeText={(text) => setStartDate(text)}
      />
      <Date
        type="datetime"
        options={{ format: 'YYYY/MM/DD' }}
        placeholder="Data final (aaaa/mm/dd)"
        value={endDate}
        onChangeText={(text) => setEndDate(text)}
      />

      <Button
        loading={loading}
        onPress={handleHoliday}
        style={{ marginTop: 30, alignSelf: 'center' }}
      >
        {loading ? (
          <ActivityIndicator color="#484747" />
        ) : (
          <TextButton>Criar</TextButton>
        )}
      </Button>
    </Container>
  );
};

export default Holiday;
