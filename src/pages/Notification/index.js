/* eslint-disable import/no-extraneous-dependencies */
import React, { useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';
import axios from 'axios';
import { createFilter } from 'react-native-search-filter';
import { useNavigation } from '@react-navigation/native';

import {
  Container,
  Header,
  Title,
  Back,
  SearchFilter,
  Input,
  ContainerRegister,
  Name,
  Manager,
  Email,
  ModalNotification,
  ViewModal,
  InputModal,
  TitleModal,
  Group,
  ButtonModal,
  TextButtonModal,
} from './styles';

const filterRegisters = ['name'];

const Notification = () => {
  const navigation = useNavigation();

  const [registers, setRegisters] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [token, setToken] = useState('');
  const [visibleModal, setVisibleModal] = useState(false);
  const [titleNotification, setTitleNotification] = useState('');
  const [body, setBody] = useState('');

  useEffect(() => {
    async function getRegisters() {
      const response = await axios.get('http://192.168.15.9:3333/register');
      setRegisters(response.data);
    }

    getRegisters();
  }, []);

  const search = (term) => {
    setSearchTerm(term);
  };

  const filtered = registers.filter(createFilter(searchTerm, filterRegisters));

  function getToken(tokenRegister) {
    setVisibleModal(true);
    setToken(tokenRegister);
  }

  async function sendNotification() {
    const message = {
      to: token,
      sound: 'default',
      title: titleNotification,
      body,
      data: { data: 'goes here' },
      _displayInForeground: true,
    };

    const response = await axios.post(
      'https://exp.host/--/api/v2/push/send',
      JSON.stringify(message),
      {
        headers: {
          Accept: 'application/json',
          'Accept-encoding': 'gzip, deflate',
          'Content-Type': 'application/json',
        },
      }
    );

    console.log(response.data);
    setVisibleModal(!visibleModal);
  }

  return (
    <Container keyboardShouldPersistTaps="handled">
      <Header>
        <Title>Notificações</Title>
        <Back onPress={() => navigation.goBack()}>
          <FontAwesome5 name="arrow-left" size={20} color="#ffb800" />
        </Back>
      </Header>

      <SearchFilter>
        <FontAwesome5 name="search" size={17} color="#ffb800" />
        <Input
          placeholder="Digite um nome"
          value={searchTerm}
          onChangeText={(value) => search(value)}
        />
      </SearchFilter>

      <FlatList
        data={filtered}
        renderItem={({ item: register }) => (
          <ContainerRegister onPress={() => getToken(register.token)}>
            <FontAwesome
              name="bell"
              size={17}
              color="#484747"
              style={{ marginLeft: 10 }}
            />
            <View style={{ flexDirection: 'column', marginLeft: 20 }}>
              <Name>{register.name}</Name>
              <Manager>{register.manager ? 'Gestor' : 'Funcionário'}</Manager>
              <Email>{register.email}</Email>
            </View>
          </ContainerRegister>
        )}
        // eslint-disable-next-line no-underscore-dangle
        keyExtractor={(item) => item._id}
      />

      <ModalNotification
        isVisible={visibleModal}
        onBackdropPress={() => setVisibleModal(!visibleModal)}
        animationIn="slideInUp"
      >
        <ViewModal>
          <TitleModal>Enviar uma notificação</TitleModal>
          <InputModal
            placeholder="Titulo da notifição"
            value={titleNotification}
            onChangeText={(value) => setTitleNotification(value)}
          />
          <InputModal
            placeholder="Descrição da notifição"
            value={body}
            onChangeText={(value) => setBody(value)}
          />
          <Group>
            <ButtonModal onPress={sendNotification}>
              <TextButtonModal>Enviar</TextButtonModal>
            </ButtonModal>
            <ButtonModal onPress={() => setVisibleModal(!visibleModal)}>
              <TextButtonModal>Voltar</TextButtonModal>
            </ButtonModal>
          </Group>
        </ViewModal>
      </ModalNotification>
    </Container>
  );
};

export default Notification;
