import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { RectButton } from 'react-native-gesture-handler';
import { darken } from 'polished';
import Modal from 'react-native-modal';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
  padding-bottom: 30px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.Text`
  font-size: 45px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Back = styled(RectButton)``;

export const SearchFilter = styled.View`
  flex-direction: row;
  background: ${darken(0.1, '#484747')};
  height: 45px;
  align-items: center;
  padding: 10px 15px;
  margin: 20px 0;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  flex: 1;
  font-size: 15px;
  color: #ffb800;
  padding-left: 15px;
`;

export const ContainerRegister = styled.TouchableOpacity`
  background: ${darken(0.05, '#ffb800')};
  margin-bottom: 10px;
  padding: 10px;
  flex-direction: row;
  align-items: center;
`;

export const Name = styled.Text`
  font-size: 18px;
  color: #484747;
  font-weight: bold;
`;

export const Manager = styled.Text`
  color: #484747;
`;

export const Email = styled.Text`
  color: #484747;
  font-size: 14px;
`;

export const ModalNotification = styled(Modal)``;

export const ViewModal = styled.View`
  height: 300px;
  margin: 0 35px;
  background: #484747;
  padding: 15px;
  margin: 10px 0;
`;

export const TitleModal = styled.Text`
  font-size: 20px;
  color: #ffb800;
  margin: 10px 0;
  font-weight: bold;
`;

export const InputModal = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
})`
  background: ${darken(0.1, '#484747')};
  height: 50px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Group = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: 30px 40px 0;
`;

export const ButtonModal = styled.TouchableOpacity`
  width: 100px;
  height: 40px;
  background: #ffb800;
  align-items: center;
  justify-content: center;
`;

export const TextButtonModal = styled.Text`
  color: #484747;
  font-size: 18px;
  font-weight: bold;
`;
