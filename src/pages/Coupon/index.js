/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';
import { AsyncStorage, ActivityIndicator } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import { toastError, toastSucess } from '../../components/Toastify';
import api from '../../services/api';
import searchOrder from '../../services/searchOrder';

import {
  Container,
  Title,
  Header,
  Back,
  Search,
  Input,
  Button,
  TextButton,
  InputForm,
  InputFormObs,
  Order,
  TitleOrder,
  ResultOrder,
} from './styles';

const Coupon = () => {
  const navigation = useNavigation();

  const [order, setOrder] = useState('');
  const [loadingSearch, setLoadingSearch] = useState(false);
  const [show, setShow] = useState(false);
  const [result, setResult] = useState('');
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [id, setId] = useState('');
  const [desc, setDesc] = useState('');
  const [obs, setObs] = useState('');

  async function handleSearchOrder() {
    setLoadingSearch(true);
    const response = await searchOrder(order);

    if (response) {
      setResult(response);
      setShow(true);
      setLoadingSearch(false);
    }
    setLoadingSearch(false);
  }

  async function handleCreateCoupom() {
    setLoadingSubmit(true);
    const token = await AsyncStorage.getItem('token');

    const data = {
      document: result.document,
      name: result.name,
      idOrder: result.orderId,
      id,
      desc,
      obs,
      reason: '',
    };

    try {
      const response = await api.post('/coupons/create', data, {
        headers: {
          Authorization: `JWT ${token}`,
        },
      });

      console.log(response.data);
      setLoadingSubmit(false);
      toastSucess('Cupom criado com suceeso');
    } catch (err) {
      toastError('Não foi possivel criar cupom, tente novamente!');
      setLoadingSubmit(false);
    }
  }

  return (
    <Container keyboardShouldPersistTaps="handled">
      <Header>
        <Title>Cupom</Title>
        <Back onPress={() => navigation.navigate('Main')}>
          <FontAwesome5
            name="arrow-left"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Search>
        <Input
          placeholder="Numero do pedido"
          value={order}
          onChangeText={(value) => setOrder(value)}
        />
        <Button loadingSearch={loadingSearch} onPress={handleSearchOrder}>
          {loadingSearch ? (
            <ActivityIndicator color="#484747" />
          ) : (
            <TextButton>Confirmar</TextButton>
          )}
        </Button>
      </Search>

      {show === false ? (
        <></>
      ) : (
        <Order>
          <TitleOrder>Documento</TitleOrder>
          <ResultOrder>{result.document}</ResultOrder>
          <TitleOrder>Nome</TitleOrder>
          <ResultOrder>{result.name}</ResultOrder>
          <TitleOrder>Pedido</TitleOrder>
          <ResultOrder>{result.orderId}</ResultOrder>
        </Order>
      )}

      <InputForm
        placeholder="Id neoassist"
        value={id}
        onChange={(value) => setId(value)}
      />
      <InputForm
        placeholder="Desconto"
        keyboardType="numeric"
        value={desc}
        onChange={(value) => setDesc(value)}
      />
      <InputFormObs
        placeholder="Observação"
        multiline
        value={obs}
        onChange={(value) => setObs(value)}
      />

      <Button
        loadingSubmit={loadingSubmit}
        onPress={() => handleCreateCoupom}
        style={{ alignSelf: 'center', marginTop: 30, marginBottom: 100 }}
      >
        {loadingSubmit ? (
          <ActivityIndicator color="#484747" />
        ) : (
          <TextButton>Criar</TextButton>
        )}
      </Button>
    </Container>
  );
};

export default Coupon;
