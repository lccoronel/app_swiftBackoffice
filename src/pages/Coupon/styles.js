import styled from 'styled-components/native';
import Constants from 'expo-constants';
import { RectButton } from 'react-native-gesture-handler';
import { darken } from 'polished';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
  padding-bottom: 100px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Back = styled(RectButton)``;

export const Search = styled.View`
  flex-direction: row;
  margin-top: 10%;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  background: ${darken(0.1, '#484747')};
  height: 45px;
  flex: 1;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const Button = styled(RectButton)`
  background: #ffb800;
  height: 45px;
  width: 30%;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 4px;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

export const Order = styled.View`
  background: ${darken(0.1, '#ffb800')};
  margin-top: 17px;
  padding: 15px;
`;

export const TitleOrder = styled.Text`
  font-weight: bold;
  font-size: 16px;
  color: #484747;
`;

export const ResultOrder = styled.Text`
  font-size: 16px;
  color: #484747;
`;

export const InputForm = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  background: ${darken(0.1, '#484747')};
  height: 45px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;

export const InputFormObs = styled.TextInput.attrs({
  placeholderTextColor: '#ffb800',
  autoCorrect: false,
})`
  background: ${darken(0.1, '#484747')};
  height: 85px;
  margin-top: 5%;
  padding: 5px 15px;
  font-size: 15px;
  color: #ffb800;
`;
