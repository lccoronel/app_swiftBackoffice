/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { FontAwesome5 } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import {
  Container,
  Header,
  Title,
  Back,
  Option,
  TitleOption,
  Description,
} from './styles';

const Manager = () => {
  const navigation = useNavigation();

  return (
    <Container>
      <Header>
        <Title>Gestor</Title>
        <Back onPress={() => navigation.goBack()}>
          <FontAwesome5
            name="arrow-left"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Back>
      </Header>

      <Option onPress={() => navigation.navigate('Users')}>
        <TitleOption>Criar usuário</TitleOption>
        <Description>Crie um acesso pra um funcionário ou gestor</Description>
      </Option>

      <Option
        enabled={false}
        onPress={() => navigation.navigate('Notification')}
      >
        <TitleOption>Noticaçōes (desenvolvimento)</TitleOption>
        <Description>
          Aqui você pode notificar qualquer usuário desse aplicativo
        </Description>
      </Option>
    </Container>
  );
};

export default Manager;
