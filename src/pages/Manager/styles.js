import styled, { css } from 'styled-components/native';
import Constants from 'expo-constants';
import { RectButton } from 'react-native-gesture-handler';
import { darken } from 'polished';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
`;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Back = styled(RectButton)``;

export const Option = styled(RectButton)`
  background: #ffb800;
  padding: 10px;
  margin-bottom: 15px;

  ${(props) =>
    props.enabled === false &&
    css`
      background: ${darken(0.15, '#ffb800')};
    `}
`;

export const TitleOption = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #484747;
  margin-bottom: 3%;
`;

export const Description = styled.Text`
  color: #484747;
  font-size: 15px;
`;
