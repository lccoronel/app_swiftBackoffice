/* eslint-disable import/no-extraneous-dependencies */
import React, { useState, useEffect } from 'react';
import { AsyncStorage } from 'react-native';
import {
  FontAwesome5,
  MaterialCommunityIcons,
  Entypo,
} from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import {
  Container,
  Title,
  Text,
  Function,
  Group,
  Option,
  TitleButton,
  Description,
  Header,
  Logout,
  GrandOption,
  Row,
} from './styles';
import api from '../../services/api';
import getTokenExpo from '../../services/getTokenExpo';

const Main = () => {
  const navigation = useNavigation();
  const [first, setFirst] = useState('');
  const [last, setLast] = useState('');
  const [manager, setManager] = useState(false);

  useEffect(() => {
    async function codToken() {
      const token = await AsyncStorage.getItem('token');

      const response = await api.get('/users/is-manager', {
        headers: {
          Authorization: `JWT ${token}`,
        },
      });

      setFirst(response.data.firstName);
      setLast(response.data.lastName);
      setManager(response.data.isManager);

      // const name = `${response.data.firstName} ${response.data.lastName}`;

      // getTokenExpo(name, response.data.email, response.data.isManager);
    }

    codToken();
  }, []);

  function handleLogout() {
    AsyncStorage.removeItem('Token');
    navigation.navigate('Login');
  }

  return (
    <Container>
      <Header>
        <Title>Menu</Title>
        <Logout onPress={handleLogout}>
          <FontAwesome5
            name="power-off"
            size={20}
            color="#ffb800"
            style={{ marginRight: 10 }}
          />
        </Logout>
      </Header>

      <Text>
        Bem vindo, {first} {last}
      </Text>
      <Function>{manager === false ? 'Funcionário' : 'Gestor'}</Function>

      <Group>
        <Option onPress={() => navigation.navigate('Coupon')}>
          <FontAwesome5 name="sticky-note" size={25} color="#484747" />
          <TitleButton>Cupom</TitleButton>
          <Description>Procure o pedido e crie um cumpom.</Description>
        </Option>
        <Option onPress={() => navigation.navigate('Holiday')}>
          <FontAwesome5 name="calendar" size={25} color="#484747" />
          <TitleButton>Feriado</TitleButton>
          <Description>
            Cadastre um feriado, para não ser considerada na entrega.
          </Description>
        </Option>
      </Group>
      <Group>
        <Option onPress={() => navigation.navigate('QrCode')}>
          <MaterialCommunityIcons
            name="qrcode-scan"
            size={25}
            color="#484747"
          />
          <TitleButton>Mensagem</TitleButton>
          <Description>
            Passe o stastus do pedido para o cliente, por mensagem.
          </Description>
        </Option>
        <Option onPress={() => navigation.navigate('Store')}>
          <FontAwesome5 name="store" size={25} color="#484747" />
          <TitleButton>Lojas</TitleButton>
          <Description>Pesquise a loja mais próxima.</Description>
        </Option>
      </Group>

      <GrandOption
        enabled={manager}
        onPress={() => navigation.navigate('Manager')}
      >
        <Row>
          <Entypo
            name="user"
            size={25}
            color="#484747"
            style={{ marginRight: 15 }}
          />
          <TitleButton>Área do gestor</TitleButton>
        </Row>
        <Description style={{ marginTop: 7 }}>
          Se você é um gestor aqui você pode criar os acessos dos funcionários e
          notificalos.
        </Description>
      </GrandOption>
    </Container>
  );
};

export default Main;
