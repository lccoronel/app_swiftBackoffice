import styled, { css } from 'styled-components/native';
import Constants from 'expo-constants';
import { darken } from 'polished';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 0 10%;
  padding-top: ${Constants.statusBarHeight + 20};
  background: #484747;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Logout = styled(RectButton)``;

export const Title = styled.Text`
  font-size: 50px;
  font-weight: bold;
  color: #ffb800;
  margin-bottom: 3%;
`;

export const Text = styled.Text`
  color: #ffb800;
  font-size: 17px;
  margin-top: 5px;
  font-weight: bold;
  margin-bottom: 15px;
`;

export const Function = styled.Text`
  background: #ffb800;
  color: #484747;
  font-weight: bold;
  font-size: 16px;
  text-align: center;
  width: 35%;
`;

export const Group = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 15px;
`;

export const Option = styled(RectButton)`
  width: 48%;
  height: 175px;
  background: ${darken(0.05, '#ffb800')};
  padding: 12px 9px;
`;

export const TitleButton = styled.Text`
  font-weight: bold;
  font-size: 17px;
  margin-top: 8px;
  color: #484747;
`;

export const Description = styled.Text`
  font-size: 15px;
  margin-top: 5px;
  color: #484747;
  width: 100%;
`;

export const GrandOption = styled(RectButton)`
  height: 110px;
  background: ${darken(0.05, '#ffb800')};
  padding: 12px 9px;
  margin-top: 15px;
  margin-bottom: 100px;

  ${(props) =>
    props.enabled === false &&
    css`
      background: ${darken(0.15, '#ffb800')};
    `}
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
