/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import { StatusBar, Vibration } from 'react-native';
import { Notifications } from 'expo';

import Routes from './src/routes';

export default function App() {
  // const [notificationApp, setNotificationApp] = useState({});

  // useEffect(() => {
  //   // Manipular notificações recebidas ou selecionadas enquanto o aplicativo
  //   // está aberto. Se o aplicativo foi fechado e aberto, tocando no
  //   // notificação (em vez de apenas tocar no ícone do aplicativo para abri-lo),
  //   // essa função será acionada no próximo tique após o aplicativo ser iniciado
  //   // com os dados da notificação.

  //   Notifications.addListener(_handleNotification);
  // }, []);

  // function _handleNotification(notification) {
  //   Vibration.vibrate();
  //   setNotificationApp(notification);
  // }

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#484747" />
      <Routes />
    </>
  );
}
